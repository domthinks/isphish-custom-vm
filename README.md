# Requirements 

* **Git http://git-scm.com/downloads**
* **VirtualBox https://www.virtualbox.org/wiki/Downloads**
* **Vagrant** 
	* Custom VM configuration	 
	* ``git clone https://domthinks@bitbucket.org/domthinks/isphish-custom-vm.git isphish``
	* cd into vagrant directory ``cd vagrant``
	* run ``vagrant up``
	* ``vagrant ssh`` 
		* once inside the vagrant box 
		* ``cd /vagrant``
		* run python isphish.py
		* run cron job to download the verified phishing domains 
			* crontab -e 
				* ```*/30 */2 * * * /usr/bin/python /vagrant/dl_csv_file.py```	
		* ``python isphish.py``
		* go to localhost:8080/ from the host machine
		
		
# Testing: 

For testing, I only implemented two HTTP request methods, GET and POST.

I used CURL CLI calls to perform my tests for both HTTP methods.

* **GET**
	*   ```for host in {1..50}; do curl http://localhost:8080/; done;```

* ** POST**
* 	```for url in `cat urls_test.txt`; do echo $url; curl -i -X POST  -H "Content-Type: multipart/form-data" --form "url=$url"  localhost:8080/shorten; done;```





