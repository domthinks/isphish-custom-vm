from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import traceback
import cgi 
import csv
import string 
import random
import urlparse 
import time
import os
from os import curdir, sep
from mimetypes import types_map, guess_type
from os.path import join
import cgitb
from SocketServer import ThreadingMixIn, ForkingMixIn
import threading 
cgitb.enable(display=0, logdir="/vagrant/isphishlogs/")

class webserverHandler(BaseHTTPRequestHandler):
    shorten_urls_all = {}

    def do_GET(self):
        try:
            parsed_path = urlparse.urlparse(self.path)

            filename, extension = os.path.splitext(self.path)
            if (self.path.endswith("/") or self.path.endswith("/shorten")):
                self.send_response(200)
                self.send_header('Content-type', guess_type(self.path))
                self.end_headers()
                output = ""
                output += "<html><head>"
                output += "<title> Shorten URL</title>"
                output += "<link rel='stylesheet' href='/static_files/form_validate.css'>"
                output += "</head>"
                output += "<body>"
                output += "<label for='url'>Shorten</label>"
                output += "<form id='shortform' method='POST' enctype='multipart/form-data' action ='/shorten'>" 
                output += "<input name='url' type='text' class = 'left' id='url' placeholder='Enter a link to shorten'>" 
                output += "<br>"
                output += "<br>"
                output += "<input type='submit' value='Submit'></form>"
                output += "<script src='/static_files/jquery-1.11.1.min.js'></script>"
                output += "<script src='/static_files/jquery.validate.min.js'></script>"
                output += "<script src='/static_files/additional-methods.min.js'></script>"
                output += "<script>"
                output +="window.onload = function(){"
                output += "jQuery.validator.setDefaults({\
                        debug: true,\
                        success: 'valid url'\
                        });\
                        $('#shortform').validate({\
                        submitHandler: function(form){ form.submit();},\
                        rules: {\
                            url: { \
                            required: true,\
                            url: true \
                            }\
                        },\
                    });\
                    };</script>"

                output += "</body></html>"

                self.wfile.write(output)
                thread_message =  threading.currentThread().getName()
                print thread_message 
                print "\n"
                #ouput+= thread_message
                print output
                return

            elif extension in (".js", ".min.js", ".css","gif"):
                static_files = join(curdir,self.path)
                headerfile = open(curdir+sep+self.path)
                self.send_response(200)
                
                self.send_header('Content-type', types_map[extension])
                self.end_headers()
                self.wfile.write(headerfile.read())
                headerfile.close()
                return
                
            if len(parsed_path.path) == 8:
                redirect_url = ''
                # if webserverHandler.shorten_urls_all is not empty 
                short_url_request = parsed_path.path.split("/")
                #check if shorten_url is in keys 
                if webserverHandler.shorten_urls_all:
                    #not empty
                    list_of_keys = webserverHandler.shorten_urls_all.viewkeys()
                    if short_url_request[1] in list_of_keys:
                       redirect_url = webserverHandler.shorten_urls_all[short_url_request[1]] 
                       self.send_response(301)
                       self.send_header("Location", redirect_url)
                       self.end_headers()
                       return None 
                    else:
                        # send a custom 404 page since resource does not exist 
                        self.send_error(404, "URL {0} not found ".format(parsed_path[2]))
                        return
                else:
                    #empty 

                    self.send_error(404, "URL {0} not found".format(parsed_path[2]))
                    return
            else:
                output =""

                parsed_path = urlparse.urlparse(self.path)
                
                self.send_error(404, "File Not Found %s" % self.path + output)

        except IOError:
            static_files = join(curdir,self.path)
            self.send_error(404, "File Not Found %s" % static_files)
            #self.send_error(500,traceback.format_exc())
    def do_POST(self):
        try:
            rando_url = None
            if self.path.endswith("/shorten"):
                ctype, pdict = cgi.parse_header(self.headers.getheader('Content-type'))

                if ctype == 'multipart/form-data':
                    fields = cgi.parse_multipart(self.rfile, pdict)
                    requestbody = fields.get('url')
                    netloc =""
                else:
                    # this is to handle non-multipart/form-data curl and other
                    # requests
                    self.send_response(200)
                    self.send_header('Content-type', 'text/html')
                    self.end_headers()
                    output =""
                    output =+" Please use multipart/form-data request header"
                    output =+"<br>"
                    output += self.html_form_string()
                    output +="</body></html>"

                    self.wfile.write(output)
                    thread_message =  threading.currentThread().getName()
                    print thread_message 
                    print "\n"
                    #ouput+= thread_message
                    print output
                    return  

                if requestbody[0] and not requestbody[0].isspace():
                    #strips out the http://
                    netloc = self.process_url_request(requestbody[0])
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                output = ""
                output += self.html_form_string()

                if netloc and not netloc.isspace():
                    url_phish_info = find_domain(url=requestbody[0])

                if url_phish_info is not None and url_phish_info['online']=='yes':
                    phish_url_details = self.verified_phish_details(url_phish_info)
                    output += phish_url_details
                else:
                    #check if url already has a generated shorten version
                    if webserverHandler.shorten_urls_all:
                        #not enmpty 
                        for short, url in webserverHandler.shorten_urls_all.iteritems():
                            if url == requestbody[0]:
                                rando_url = short
                            else:
                                rando_url = self.random_url_suffix_generator()
                                #break
                        output+= self.legitimate_url(rando_url,requestbody[0])

                    else:
                        #empty 
                        rando_url = self.random_url_suffix_generator()
                        output+= self.legitimate_url(rando_url,requestbody[0])

                output += "</body></html>"

                try:
                    self.wfile.write(output)

                except:
                    self.send_error(404, "something bad happend " % self.path)
                    parsed_path = urlparse.urlparse(self.path)

                    output+="\n <h1>urlparse info {0} </h1>\n".format(parsed_path) 

                thread_message =  threading.currentThread().getName()
                print thread_message 
                print "\n"
                #ouput+= "POST: "+thread_message
                print output

            else:
                self.send_error(404, "File Not Found %s" % self.path)
        except:
            self.send_error(500,traceback.format_exc())

    def html_form_string(self):
        output =""
        output += "<html><head>"
        output += "<title> Shorten URL</title>"
        output += "<link rel='stylesheet' href='/static_files/form_validate.css'>"
        output += "</head>"
        output += "<body>"
        output += "<label for='url'>Shorten</label>"
        output += "<form id='shortform' method='POST' enctype='multipart/form-data' action ='/shorten'>" 
        output += "<input name='url' type='text' class = 'left' id='url' placeholder='Enter a link to shorten'>" 
        output += "<br>"
        output += "<br>"
        output += "<input type='submit' value='Submit'></form>"
        output += "<script src='/static_files/jquery-1.11.1.min.js'></script>"
        output += "<script src='/static_files/jquery.validate.min.js'></script>"
        output += "<script src='/static_files/additional-methods.min.js'></script>"
        output += "<script>"
        output +="window.onload = function(){"
        output += "jQuery.validator.setDefaults({\
                debug: true,\
                success: 'valid url'\
                });\
                $('#shortform').validate({\
                submitHandler: function(form){ form.submit();},\
                rules: {\
                    url: { \
                    required: true,\
                    url: true \
                    }\
                },\
            });\
            };</script>"
        return output

    def process_request(self,requestpayload):
        netloc = None
        data = []
        phishtankapi_file = "/vagrant/tlds-alpha-by-domain.txt"

        domain = requestpayload.split("/")
        if domain != 1:
            for tokens in domain:
                s = tokens.find(".")
                if s !=-1:
                    pos = tokens.find(".")
                    netloc = str(tokens)
        return netloc

    def process_url_request(self, requestpayload):
        netloc = None
        if requestpayload !="":
            url_components = urlparse.urlparse(requestpayload)
            netloc = url_components[1] 
    
        return netloc 
    
    def random_url_suffix_generator(self):
        ascii_letters_digits= string.ascii_letters+string.digits
        rando = random.sample(ascii_letters_digits,7)
        random_path = "".join(map(str,rando))
        return random_path
    def verified_phish_details(self, phishing_details=None):
        output = ""
        if phishing_details is not None:
            output += "<h1> This domain has been flaged as a phish by PhishTank</h1>"
            output += "<hr>"
            output += "Phishing url:"
            output += " <b>DO NOT VISIT SITE </b>%s" %phishing_details['url']
            #output += "<a href='%s'> %s</a>" %(phishing_details['phish_detail_url'],phishing_details['url'])
            output += "<br>"
            output += "<div class ='phish_details'>"
            output +="<span class ='phishtank_url'>"
            output += "Click on the link below to find our more about the domain"
            output += "<a href='%s'> Phishing Details </a>" %phishing_details['phish_detail_url']
            output += "</div>"

        return output

    def legitimate_url(self, random_url_suffix, original_url=None,):
        output =""
        if original_url is not None and original_url !='':
            output += "<h1>Not a Phish</h1>"
            output += "<hr>"
            output += "<br>Created shorten URL below <br>"
            output += "<div class ='shorten_details'>"
            output +="<span class ='shorten_url'>"
            output += "You may now use the shorten version fo the link "
            output += "<a href='%s/%s'> " % (self.headers['Host'], random_url_suffix)
            output  += " %s/%s</a><br>" % (self.headers['Host'],random_url_suffix)
            output += "<br>Original URL: <a href='%s'> %s </a>"%(original_url,original_url) +"</a><br>"
            
            if webserverHandler.shorten_urls_all:
                #not empty
                list_of_keys = webserverHandler.shorten_urls_all.viewkeys()
                list_of_values = webserverHandler.shorten_urls_all.viewvalues()
                # check if random generated shorten_url is in key.
                # if key is in here, then do not update it
                if random_url_suffix not in list_of_keys or  original_url not in list_of_values:
                    webserverHandler.shorten_urls_all.update({random_url_suffix:original_url})
            else:
                #empty, so just add it 
                #webserverHandler.shorten_urls_all[random_url_suffix] !=original_url:
                webserverHandler.shorten_urls_all.update({random_url_suffix:original_url})
            output += "</div>"
            #TODO: Add shorten URL and original URL to databse

        return output
    #def dict_of_random_urls(self, short_urls_dict):
    #    shorten_urls_all.update

# takes url, strips http:// and search csv file for the domain
# also strips http:// from all domains in csv file 
def find_domain(url):
    phishy_details =  None
    url_to_check = None
    if url and not url.isspace():
        url_parsed = urlparse.urlparse(url)
        if url_parsed[1].find("www.") != -1:
                #www. found  
                url_parsed_no_www = (url_parsed[1].split("www."))[1]
                url_to_check = url_parsed_no_www
        else:
           url_to_check = url_parsed[1]

        with open('verified_online.csv') as  csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                row_url_parsed = urlparse.urlparse(row['url'])
                if row_url_parsed[1].find("www.") != -1:
                    print "row url parsed {0} \n" .format(row_url_parsed[1]) 
                    print "input url parsed {0} \n" .format(url_to_check)
                    row_url_no_www = (row_url_parsed[1].split("www."))[1]
                    print "no www for row %s" %row_url_no_www 
                    if str(row_url_no_www) == str(url_to_check):
                        phishy_details = row
                        break
                elif row_url_parsed[1].find("www.") == -1:
                    if str(row_url_parsed[1]) == str(url_to_check):
                        phishy_details = row
    return phishy_details

class ThreadHTTPServer(ThreadingMixIn, HTTPServer):
    "handle multiple threads"
    pass


def main():
    try:
        host_name = 'smalpickin.com'
        port = 8080
        server = ThreadHTTPServer(('', port), webserverHandler)
        #server = HTTPServer(('',port), webserverHandler)
        server.serve_forever()
    except KeyboardInterrupt:
        #throw an error
        #webserverHandler.shorten_urls_all = {}
        print "^C entered, web server stopped running at ort %s\n" %port
        server.socket.close()

if __name__ == '__main__':
    main()